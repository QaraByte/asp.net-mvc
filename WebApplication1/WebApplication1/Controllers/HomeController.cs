﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private Models.citiesDBEntities db = new Models.citiesDBEntities();
        //private IndexContext db2 = new IndexContext();

        public ActionResult Index()
        {
            var Items = db.PostIndexes;
            //var item2 = db2.PostIndexes;
            //ViewBag.Indexes = item2;
            return View(Items);
        }

        public ActionResult IndexPage(int item_id)
        {
            var item = db.PostIndexes.FirstOrDefault(x => x.Id == item_id);
            return View(item);
        }

        public string AddIndex(PostIndex postIndex)
        {
            postIndex.postIndex1 = "1212";
            db.PostIndexes.Add(postIndex);
            db.SaveChanges();
            return postIndex.city;

        }
    }
}